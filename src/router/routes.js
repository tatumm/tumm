
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'tum', component: () => import('pages/tum.vue') },
      { path: 'learn1', component: () => import('pages/tum1.vue') },
      { path: 'h1', component: () => import('pages/page1.vue') },
      { path: 'h2', component: () => import('pages/page2.vue') },
      { path: 'h3', component: () => import('pages/page3.vue') },
      { path: 'h4', component: () => import('pages/page4.vue') },
      { path: 'h5', component: () => import('pages/page5.vue') },
      { path: 'test', component: () => import('pages/test.vue') },
      { path: 'test1', component: () => import('pages/tested.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
